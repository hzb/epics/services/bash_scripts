#!/bin/bash

# Get the input parameters

IOC=$1
IOC_REPO=$2
IOC_TAG=$3
IOC_FOLDER_NAME=$4

echo "IOC: $IOC"
echo "IOC_REPO: $IOC_REPO"
echo "IOC_TAG: $IOC_TAG"
echo "IOC_FOLDER_NAME: $IOC_FOLDER_NAME"

if [ -d "/opt/epics/support/areaDetector/$4" ]; then
	echo "Directory areaDetector/$4 already exists. Removing it..."
	rm -rf "/opt/epics/support/areaDetector/$4"
fi

git clone --depth 1 --recursive --branch "$3" "$2" "/opt/epics/support/areaDetector/$4"
git -C "/opt/epics/support/areaDetector/$4" config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
git -C "/opt/epics/support/areaDetector/$4" fetch
# Make IOC boot scripts executable
find "/opt/epics/support/areaDetector/$4/iocBoot" -type f -name "*.cmd*" -exec chmod +x {} + || true

echo "Adding area detector for $IOC_FOLDER_NAME"
sed -i "s|#\(.*$IOC_FOLDER_NAME\)|\1|" /opt/epics/support/areaDetector/configure/RELEASE.local

# Create RELEASE.local in the IOC's configure directory
echo "EPICS_BASE=/opt/epics/base" >"/opt/epics/support/areaDetector/$IOC_FOLDER_NAME/configure/RELEASE.local"
