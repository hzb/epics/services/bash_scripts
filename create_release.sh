#!/bin/bash

# This script creates and manages RELEASE.local files for EPICS modules.
# It reads module configurations from a YAML file and generates appropriate RELEASE.local entries,
# handling module dependencies and special cases.

# Define configuration files
MODULES_FILE="/opt/epics/modules.yml" # YAML file containing module configurations
RELEASE_LOCAL_FILE="RELEASE.local"    # Output RELEASE.local file

# Extract folder_name values for all modules from YAML config
# Returns: Array of folder names for all configured modules
extract_all_dbd_values() {
	local modules=($(yq eval '.modules | keys | .[]' "$MODULES_FILE"))
	local dbd_values=()

	for module in "${modules[@]}"; do
		local dbd=$(yq e ".modules.${module}.folder_name" "$MODULES_FILE")
		dbd_values+=("$dbd")
		echo "$dbd"
	done

	echo "${dbd_values[@]}"
}

# Update RELEASE.local file with module path if folder name matches
# Args:
#   $1: Folder name to check
#   $2: Module name to add
update_release_local() {
	local folder_name=$1
	local module_name=$2

	if [[ $folder_name == *"$module_name"* ]] && [[ "$module_name" != "ADCore" && "$module_name" != "ADSupport" ]]; then
		echo "$(echo "${module_name}" | tr '[:lower:]' '[:upper:]')=/opt/epics/support/$module_name" >>"$RELEASE_LOCAL_FILE"
	fi
}

# Add module undefine statement if not already present
# Args:
#   $1: Module name to process
add_undefine_if_not_added() {
	local module_name=$1
	local undefine_added=false
	# Extract .dbd values for all modules
	all_dbd_values=($(extract_all_dbd_values))
	# Update RELEASE.local based on folder_name
	for value in "${all_dbd_values[@]}"; do
		if [[ $value == *"$module_name"* ]]; then
			echo "folder_name: $value"
			update_release_local "$value" "$module_name"
			undefine_added=true
			break
		fi
	done

	# Add "undefine" if not added already
	if [ "$undefine_added" = false ]; then
		echo "undefine $(echo "${module_name}" | tr '[:lower:]' '[:upper:]')" >>"$RELEASE_LOCAL_FILE"
	fi
}

# Add module path to RELEASE.local if not already present
# Args:
#   $1: Module name to add
add_module_if_not_added() {
	local module_name=$1

	if [[ "$module_name" == "ADCore" || "$module_name" == "ADSupport" ]]; then
		echo "$(echo "${module_name}" | tr '[:lower:]' '[:upper:]')=/opt/epics/support/areaDetector/$module_name" >>"$RELEASE_LOCAL_FILE"
	fi

	if ! grep -q "^$(echo "${module_name}" | tr '[:lower:]' '[:upper:]')" "$RELEASE_LOCAL_FILE" && [[ "$module_name" != "ADCore" && "$module_name" != "ADSupport" ]]; then
		echo "$(echo "${module_name}" | tr '[:lower:]' '[:upper:]')=/opt/epics/support/$module_name" >>"$RELEASE_LOCAL_FILE"
	fi
}

# Extract .dbd values for all modules
all_dbd_values=($(extract_all_dbd_values))

echo "EPICS_BASE=/opt/epics/base" >RELEASE.local
echo "SUPPORT=/opt/epics/support" >>RELEASE.local

# Track which modules have been processed to avoid duplicates
processed_modules=()

# Process all modules except special cases
for module_name in "${all_dbd_values[@]}"; do
	case "$module_name" in
	"sscan" | "seq" | "pcre")
		# Skip these modules
		;;
	*)
		# Add other modules
		if ! [[ "${processed_modules[*]}" =~ "$module_name" ]]; then
			add_undefine_if_not_added "$module_name"
			add_module_if_not_added "$module_name"
			processed_modules+=("$module_name")
		fi
		;;
	esac
done

# Handle special case modules
add_undefine_if_not_added "sscan"
add_undefine_if_not_added "seq"

# Fix module name inconsistencies
sed -i 's/\<SEQ\>/SNCSEQ/' "RELEASE.local"
sed -i 's/\<MOTORPHYTRON\>/MOTOR_PHYTRON/' "RELEASE.local"
add_undefine_if_not_added "pcre"
# Fix areaDetector naming
sed -i 's/\<AREADETECTOR\>/AREA_DETECTOR/' "RELEASE.local"

cp RELEASE.local /opt/epics/ioc/RELEASE.local
cp RELEASE.local /opt/epics/support/RELEASE.local
