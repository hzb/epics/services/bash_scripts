# EPICS Installation Scripts

This documentation covers the shell scripts used for installing and configuring EPICS IOCs and modules.

## Overview

The installation system consists of three main scripts:

1. `install_modules.sh` - Installs EPICS support modules
2. `install_ioc.sh` - Installs and configures EPICS IOCs
3. `create_release.sh` - Generates RELEASE.local configuration files

## Prerequisites

- Linux/Unix environment with Bash shell
- EPICS base installed in `/opt/epics/base`
- `yq` command-line tool for YAML processing
- Git for repository operations
- Write permissions in `/opt/epics/` directory

## Configuration File

All scripts use a central YAML configuration file located at `/opt/epics/modules.yml`. This file defines:

- Support modules to be installed
- IOC configuration
- Version/tag information
- Repository locations

Example configuration structure:
```yaml
modules:
  asyn:
    repo: "https://github.com/epics-modules/asyn"
```

## Install Modules Script

### Purpose
`install_modules.sh` handles the installation of EPICS support modules.

### Features
- Automated module installation from Git repositories
- Version control through specified tags
- Special handling for specific modules (e.g., asyn configuration)
- Parallel compilation using available CPU cores

### Usage
```bash
./install_modules.sh
```

### Process Flow
1. Reads module definitions from YAML
2. For each module:
   - Clones repository with specified tag
   - Applies module-specific configurations
   - Compiles using make

## Install IOC Script

### Purpose
`install_ioc.sh` manages IOC installation and configuration.

### Features
- Automated IOC repository cloning
- Database file configuration
- Protocol file management
- Module dependency handling
- Build system setup

### Usage
```bash
./install_ioc.sh
```

### Key Functions
- **Repository Management**
  - Clones IOC repository
  - Configures Git settings
  - Makes boot scripts executable

- **Build Configuration**
  - Updates Makefiles with database files
  - Configures module dependencies
  - Handles protocol files

- **Output**
  - Creates `/opt/epics/ioc_path.txt` with IOC startup location
  - Compiles IOC application

## Create Release Script

### Purpose
`create_release.sh` generates EPICS RELEASE.local configuration files.

### Features
- Automatic dependency management
- Special handling for specific modules
- Multiple output locations
- Case-sensitive naming corrections

### Usage
```bash
./create_release.sh
```

### Output Files
Creates RELEASE.local in:
- Current directory
- `/opt/epics/ioc/`
- `/opt/epics/support/`

### Configuration Example
```bash
EPICS_BASE=/opt/epics/base
SUPPORT=/opt/epics/support
ASYN=/opt/epics/support/asyn
MOTOR=/opt/epics/support/motor
```

## Troubleshooting

### Common Issues

1. **Missing Configuration File**
   ```bash
   Error: /opt/epics/modules.yml not found
   ```
   Solution: Create or copy the YAML configuration file to the correct location

2. **Git Clone Failures**
   ```bash
   fatal: repository not found
   ```
   Solution: 
   - Verify repository URL
   - Check network connectivity
   - Ensure tag exists

3. **Build Errors**
   ```bash
   make: *** [Makefile:xxx] Error 1
   ```
   Solution:
   - Check EPICS base installation
   - Verify all dependencies are installed
   - Review module compatibility

### Best Practices

1. **Configuration Management**
   - Keep YAML file under version control
   - Document any custom modifications
   - Use consistent naming conventions

2. **Installation Process**
   - Run scripts in order: modules → release → IOC
   - Monitor script output for errors
   - Backup existing configurations before updates

3. **Maintenance**
   - Regularly update module versions
   - Test configuration changes in development
   - Maintain documentation of custom modifications

## Support

For issues or questions:
1. Check script output for error messages
2. Review EPICS module documentation
3. Consult your facility's EPICS support team
4. Check EPICS community resources

## References

- [EPICS Documentation](https://epics.anl.gov/docs/)
- [EPICS Modules](https://epics.anl.gov/modules/)
- [YQ Documentation](https://mikefarah.gitbook.io/yq/)

