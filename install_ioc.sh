#!/bin/bash

# This script installs and configures an EPICS IOC based on configuration from a YAML file.
# It handles cloning the IOC repository, setting up build files, and compiling the IOC.

# Define the input YAML configuration file path
MODULES_FILE="/opt/epics/modules.yml"

# Function to display error messages and exit
# Args:
#   $1: Error message to display
die() {
	echo "Error: $1" >&2
	exit 1
}

# Check if a file exists
# Args:
#   $1: Path to file to check
check_file_existence() {
	[ -f "$1" ] || die "$1 not found."
}

# Extract IOC configuration values from YAML file
# Args:
#   $1: Path to YAML file
# Returns: Comma-separated string of folder_name,source,tag,user[,ad]
extract_ioc_values() {
	yq e ".ioc.folder_name + \",\" + .ioc.source + \",\" + .ioc.tag + \",\" + .ioc.user" "$1"
}

# Clone IOC repository and set up git configuration
# Args:
#   $1: Base directory path
#   $2: Git repository URL
#   $3: Git tag/branch to checkout
#   $4: IOC folder name
retrieve_and_copy_source_files() {
	# If directory exists, remove it first
	if [ -d "$1/${4}IOC" ]; then
		echo "Directory $1/${4}IOC already exists. Removing it..."
		rm -rf "$1/${4}IOC"
	fi

	git clone --depth 1 --recursive --branch "$3" "$2" "$1/${4}IOC"
	git -C "$1/${4}IOC" config remote.origin.fetch "+refs/heads/*:refs/remotes/origin/*"
	git -C "$1/${4}IOC" fetch
	# Make IOC boot scripts executable
	find "$1/${4}IOC/iocBoot" -type f -name "*.cmd*" -exec chmod +x {} + || true
}

# Update IOC Makefile with database files
# Args:
#   $1: Base directory path
#   $2: IOC name
update_makefile_with_files() {
	local db_path="$1/${2}IOC/${2}IOCApp/Db"
	local makefile_path_db="$1/${2}IOC/${2}IOCApp/Db/Makefile"

	# Find all database and template files
	local files=$(find "$db_path" -type f \( -name "*.db" -o -name "*.req" -o -name "*.sub*" -o -name "*.template" \))
	if [ -z "$files" ]; then
		echo "No .db or .sub* files found in the specified folder."
	fi

	# Add each file to the Makefile
	for file in $files; do
		echo "Adding $file to Makefile..."
		base_file="$(basename "$file")"

		# Handle .template files specially
		if [[ $base_file == *.template ]]; then
			target_file="${base_file%.template}.db"
		else
			target_file="$base_file"
		fi

		sed -i "/^#DB += xxx.db/ a\\DB += $target_file" "$makefile_path_db"
	done
}

# Move protocol files to protocols directory
# Args:
#   $1: IOC base path
check_for_proto_files_in_db() {
	local ioc_path="$1"
	local protocols_path="$ioc_path/protocols"

	mkdir -p "$protocols_path"

	local files=$(find "$ioc_path" -type f -name "*.proto")

	if [ -z "$files" ]; then
		echo "No .proto files found in the specified folder."
	else
		echo ".proto files found. Moving to $protocols_path:"
		echo "$files"
		while IFS= read -r file; do
			mv "$file" "$protocols_path"
		done <<<"$files"
		echo "Files moved successfully."
	fi
}

# Update Makefile with module dependencies
# Args:
#   $1: Path to YAML config file
#   $2: Base directory path
#   $3: IOC name
# Update Makefile with module dependencies by adding required libraries and DBD files
# Args:
#   $1: Path to YAML config file containing module definitions
#   $2: Base directory path where IOC is located
#   $3: IOC name used in Makefile variables
update_makefile_with_dbd_values() {
	# Get list of all module names from YAML config
	local modules=($(yq eval '.modules | keys | .[]' "$1"))
	# Path to IOC's src Makefile that needs updating
	local makefile_path_src="$2/${3}IOC/${3}IOCApp/src/Makefile"

	# Process module dependencies in reverse order
	local dbd_values=()
	for ((i = ${#modules[@]} - 1; i >= 0; i--)); do
		local module="${modules[$i]}"
		# Skip ipac module as it's handled specially
		if [ "$module" != "ipac" ]; then
			# Get DBD and library values for this module from YAML
			local dbd=$(yq e ".modules.${module}.dbd" "$1")
			local lib=$(yq e ".modules.${module}.lib" "$1")

			# Add library dependency after EPICS base libs
			sed -i '/'"${3}_LIBS += \$(EPICS_BASE_IOC_LIBS)"'/ a\'"${3}_LIBS += ${lib}" "$makefile_path_src"

			# Split space-separated DBD values into array and add to collection
			IFS=' ' read -r -a dbd_values_array <<<"$dbd"
			dbd_values+=("${dbd_values_array[@]}")

			if [ "$module" = "pydevice" ]; then
				# Add PyDevice config include line after TOP config
				sed -i '/include \$(TOP)\/configure\/CONFIG/ a\include \$(PYDEVICE)\/configure\/CONFIG.PyDevice' "$makefile_path_src"
			fi
		fi
	done

	# Iterate through each DBD value and add it to the Makefile
	# For each non-empty DBD value:
	# - Use sed to insert a new line after the comment line "#<ioc>_DBD += xxx.dbd"
	# - The new line adds the DBD dependency in the format "<ioc>_DBD += <dbd_value>"
	# This adds all required DBD files that the IOC depends on from the support modules
	for dbd_value in "${dbd_values[@]}"; do
		if [ -n "$dbd_value" ]; then
			sed -i "/#${3}_DBD += xxx.dbd/ a\\${3}_DBD += $dbd_value" "$makefile_path_src"
		fi
	done
}

# Main script execution

# Verify config file exists
check_file_existence "$MODULES_FILE"

# Get IOC configuration
IOC_INFO=$(extract_ioc_values "$MODULES_FILE")

# Parse IOC configuration values
IFS=',' read -r IOC_FOLDER_NAME IOC_REPO IOC_TAG IOC_USER <<<"$IOC_INFO"

# Display configuration
echo "IOC_FOLDER_NAME: $IOC_FOLDER_NAME, IOC_REPO: $IOC_REPO"
echo "IOC_TAG: $IOC_TAG, IOC_USER: $IOC_USER"

# Check if IOC name starts with AD or if IOC_AD is set
if [[ "$IOC_FOLDER_NAME" = AD* ]]; then
	./add_area_detector.sh "$IOC" "$IOC_REPO" "$IOC_TAG" "$IOC_FOLDER_NAME"

	# Save IOC startup script path
	echo "IOC_ST_CMD_PATH=/opt/epics/support/areaDetector/$IOC_FOLDER_NAME/iocBoot/ioc${IOC_FOLDER_NAME}" >/opt/epics/ioc_path.txt

	# Build the IOC
	cd /opt/epics/support/areaDetector
	make clean
	make
else
	# Set up IOC source code
	retrieve_and_copy_source_files "$IOC" "$IOC_REPO" "$IOC_TAG" "$IOC_FOLDER_NAME"

	# Configure protocol files
	check_for_proto_files_in_db "$IOC/${IOC_FOLDER_NAME}IOC"

	# Update Makefiles
	update_makefile_with_files "$IOC" "$IOC_FOLDER_NAME"
	update_makefile_with_dbd_values "$MODULES_FILE" "$IOC" "$IOC_FOLDER_NAME"

	# Save IOC startup script path
	echo "IOC_ST_CMD_PATH=$IOC/${IOC_FOLDER_NAME}IOC/iocBoot/ioc${IOC_FOLDER_NAME}" >/opt/epics/ioc_path.txt

	# Build the IOC
	cd $IOC/${IOC_FOLDER_NAME}IOC
	make clean
	make
fi
