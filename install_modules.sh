#!/bin/bash

# Define the input file
MODULES_FILE="/opt/epics/modules.yml"

# Check if the configuration file exists before proceeding
if [ ! -f "$MODULES_FILE" ]; then
	echo "Error: $MODULES_FILE not found."
	exit 1
fi

# Parse and process each module defined in the YAML file
# The yq command extracts key-value pairs from the modules section
# For each module, we:
# - Clone the repository with specified tag
# - Apply any module-specific configuration
# - Build the module
while IFS=": " read -r key value || [ -n "$key" ]; do
	if [[ $key == "repo" ]]; then
		repo=$value
	elif [[ $key == "tag" ]]; then
		tag=$value
	elif [[ $key == "folder_name" ]]; then
		folder_name=$value
	elif [[ $key == "dbd" ]]; then
		dbd=$value
		# Perform actions here with the extracted values

		echo "Cloning $repo with tag $tag into folder $folder_name"
		if [[ $folder_name == area* ]]; then
			# Create areaDetector directory if it doesn't exist
			mkdir -p "${SUPPORT}/areaDetector"
			git clone --depth 1 --branch "$tag" "$repo" "${SUPPORT}/${folder_name}"

			# Only try to configure if the configure directory exists
			if [ -d "${SUPPORT}/areaDetector/configure" ]; then
				echo "Configuring areaDetector"
				cp ${SUPPORT}/areaDetector/configure/EXAMPLE_RELEASE.local ${SUPPORT}/areaDetector/configure/RELEASE.local
				cp /opt/epics/support/RELEASE.local ${SUPPORT}/areaDetector/configure/RELEASE_LIBS.local
				cp ${SUPPORT}/areaDetector/configure/EXAMPLE_CONFIG_SITE.local ${SUPPORT}/areaDetector/configure/CONFIG_SITE.local
				cp /opt/epics/support/RELEASE.local ${SUPPORT}/areaDetector/configure/RELEASE_PRODS.local
			else
				echo "Warning: ${SUPPORT}/areaDetector/configure directory not found"
			fi
		elif [[ $folder_name == AD* ]]; then
			# For modules starting with AD, clone into areaDetector subdirectory
			git clone --depth 1 --branch "$tag" "$repo" "${SUPPORT}/areaDetector/${folder_name}"
			cp /opt/epics/support/RELEASE.local ${SUPPORT}/areaDetector/${folder_name}/configure
		else
			# For all other modules, clone directly into SUPPORT
			git clone --depth 1 --branch "$tag" "$repo" "${SUPPORT}/${folder_name}"
		fi

		if [ "$folder_name" = "asyn" ]; then
			echo "Modifying asyn configuration"
			sed -i '/TIRPC/s/^#//g' "${SUPPORT}/${folder_name}/configure/CONFIG_SITE"
		fi
		if [[ $folder_name == AD* ]]; then
			make -C ${SUPPORT}/areaDetector -j $(nproc)
		else
			make -C ${SUPPORT}/${folder_name} -j $(nproc)
		fi
	fi

done < <(yq eval '.modules | to_entries | .[] | .value | to_entries | .[] | .key + ": " + .value' $MODULES_FILE)
